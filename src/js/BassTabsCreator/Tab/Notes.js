
const Notes = class {
  #values = []
  #string = null

  static emptyFor(string) {
    return new Notes(string)
  }

  constructor(string) {
    this.#string = string
  }

  add(note) {
    this.#values.push(note)
  }

  removeLastNote() {
    this.#values.pop()
  }

  asString() {
    return this.#string + '|' + this.#values.join('') + '\n'
  }

  isEmpty() {
    return (this.#values.length === 0)
  }
}
