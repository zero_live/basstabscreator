
const Tab = class {
  #notes = {
    E: Notes.emptyFor('E'),
    A: Notes.emptyFor('A'),
    D: Notes.emptyFor('D'),
    G: Notes.emptyFor('G')
  }

  addRest() {
    this.#fillAllStringsWith(this.#restAnnotation())
  }

  addEnd() {
    this.#fillAllStringsWith(Annotations.end())
  }

  addNote({ string, fret }) {
    this.#notes[string].add(fret)

    this.#fillAllStringWithRestInstead(string, fret)
  }

  removeLastNote() {
    for (const string in this.#notes) {
      this.#notes[string].removeLastNote()
    }
  }

  asString() {
    const gString = this.#notes.G.asString()
    const dString = this.#notes.D.asString()
    const aString = this.#notes.A.asString()
    const eString = this.#notes.E.asString()

    return gString + dString + aString + eString
  }

  isEmpty() {
    return Object.values(this.#notes).every(note => note.isEmpty())
  }

  #fillAllStringWithRestInstead(stringToAvoid, fret) {
    for (const string in this.#notes) {
      if (string !== stringToAvoid) {
        this.#notes[string].add(this.#restAnnotation())

        if (this.#isTwoDigits(fret)) {
          this.#normalizeSize(string)
        }
      }
    }
  }

  #fillAllStringsWith(annotation) {
    for (const string in this.#notes) {
      this.#notes[string].add(annotation)
    }
  }

  #normalizeSize(string) {
    const normalization = ' '

    this.#notes[string].add(normalization)
  }

  #restAnnotation() {
    return Annotations.rest()
  }

  #isTwoDigits(fret) {
    return (fret >= 10)
  }
}
