
const BassTabsCreator = class {
    static forGuitarBassFourStrings() {
      return new BassTabsCreator()
    }

    #tab = null

    constructor() {
      this.#tab = new Tab()
    }

    addRest() {
      this.#tab.addRest()

      return this
    }

    addEnd() {
      this.#tab.addEnd()

      return this
    }

    addNote(note) {
      this.#tab.addNote(note)

      return this
    }

    removeLastNote() {
      this.#tab.removeLastNote()

      return this
    }

    createTab() {
      return this.#tab
    }

    isEmpty() {
      return this.#tab.isEmpty()
    }
  }
