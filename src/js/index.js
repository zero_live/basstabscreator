
const tabArea = document.querySelector('#tab')
const stringText = document.querySelector('#string')
const fretText = document.querySelector('#fret')
const creator = BassTabsCreator.forGuitarBassFourStrings()
tabArea.value = creator.createTab().asString()
const creators = [creator]
let currentCreator = 0
let string = ''
let fret = ''

document.addEventListener('keydown', (event) => {
  if (event.key === '-') { creators[currentCreator].addRest() }
  if (event.key === '|') { creators[currentCreator].addEnd() }
  if (event.key === 'Backspace') {
    if (creators[currentCreator].isEmpty()) {
      if (currentCreator > 0) {
        currentCreator -= 1
        creators.pop()
      }
    } else {
      creators[currentCreator].removeLastNote()
    }
  }
  if (string.length === 0) {
    if ('EADG'.includes(event.key)) {
      string = event.key
      stringText.textContent = string
    }
  } else if (fret.length === 0) {
    if ('0123456789'.includes(event.key)) {
      fret = event.key
      fretText.textContent = fret
    }
  } else if (fret.length === 1) {
    if ('0123456789'.includes(event.key)) {
      fret += event.key
      fretText.textContent = fret
    }
  }
  if (event.key === 'Enter' && string && fret) {
    creators[currentCreator].addNote({ string, fret })

    string = ''
    fret = ''
    stringText.textContent = string
    fretText.textContent = fret
  }

  if(event.key === '+') {
    const newCreator = BassTabsCreator.forGuitarBassFourStrings()

    creators.push(newCreator)
    currentCreator += 1
  }

  tabArea.value = creators.map(creator => creator.createTab().asString()).join('\n')
})

document.querySelector('#copy-to-clipboard').addEventListener('click', () => {
  const tab = tabArea.value

  navigator.clipboard.writeText(tab)
})
