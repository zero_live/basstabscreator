
describe('Tab', () => {
  it('retrieves its value as string', () => {
    const tab = new Tab()
    tab.addRest()
    tab.addNote({ string: 'E', fret: '5' })
    tab.addRest()
    tab.addNote({ string: 'A', fret: '3' })
    tab.addEnd()

    const tabAsString = tab.asString()

    expect(tabAsString).toBe('G|----|\nD|----|\nA|---3|\nE|-5--|\n')
  })

  it('retrieves decimal frets with rest and whitespace in the remaining strings', () => {
    const tab = new Tab()
    tab.addNote({ string: 'E', fret: '15' })
    tab.addEnd()

    const tabAsString = tab.asString()

    expect(tabAsString).toBe('G|- |\nD|- |\nA|- |\nE|15|\n')
  })
})
