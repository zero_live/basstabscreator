
describe('BassTabsCreator', () => {
  let creator

  beforeEach(() => {
    creator = BassTabsCreator.forGuitarBassFourStrings()
  })

  it('creates an empty tab', () => {

    const tab = creator.createTab()

    expect(tab.asString()).toBe('G|\nD|\nA|\nE|\n')
  })

  it('creates a tab with a rest', () => {

    const tab = creator.addRest().createTab()

    expect(tab.asString()).toBe('G|-\nD|-\nA|-\nE|-\n')
  })

  it('creates a tab with an end', () => {

    const tab = creator.addEnd().createTab()

    expect(tab.asString()).toBe('G||\nD||\nA||\nE||\n')
  })

  it('creates a tab with a fret in a string', () => {

    const tab = creator.addNote({ string: 'E', fret: '0' }).createTab()

    expect(tab.asString()).toBe('G|-\nD|-\nA|-\nE|0\n')
  })

  it('removes a note from the tab', () => {
    creator.addEnd()

    creator.removeLastNote()

    const tab = creator.createTab()
    expect(tab.asString()).toBe('G|\nD|\nA|\nE|\n')
  })

  it('knows if the tab is empty', () => {

    const isEmpty = creator.isEmpty()

    expect(isEmpty).toBeTruthy()
  })

  it('knows if the tab is not empty', () => {

    const isEmpty = creator.addEnd().isEmpty()

    expect(isEmpty).toBeFalsy()
  })
})
