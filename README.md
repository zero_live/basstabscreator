# Bass Tabs Creator

Simple web app for create your tabs for Guitar Bass with four strings.

## System requirements

- Browser that supports Javascript ES6

## How to run tests

- Open `spec/Verano.html` in your browser.

## How to run the project

- Open `src/index.html` in your browser.